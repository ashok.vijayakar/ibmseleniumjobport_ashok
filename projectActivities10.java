import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities10 {
	WebDriver driver;
	WebDriverWait wait;
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
		
	  }
	
	@Test
  public void CreateANewUser() throws InterruptedException {
		driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click(); 
		// Click on the "Users" menu item
				driver.findElement(By.cssSelector("a.menu-icon-users")).click();
		// Click on the "Add New" button
				driver.findElement(By.cssSelector("#menu-users > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")).click();
			 
		  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		   WebElement userName = driver.findElement(By.id("user_login"));
		   userName.sendKeys("Ashok12345");
		   WebElement email = driver.findElement(By.id("email"));
		   email.sendKeys("ashok1.vijayakar2@gmail.com");
		   WebElement firstName = driver.findElement(By.id("first_name"));
		   firstName.sendKeys("Ashok2"); 
		   WebElement lastName = driver.findElement(By.id("last_name"));
		   lastName.sendKeys("vijayakar2");
		   WebElement companyurl = driver.findElement(By.id("url"));
		   companyurl.sendKeys("www.ibm.com");
		   Thread.sleep(3000);
		 WebElement generate =  driver.findElement(By.cssSelector(".wp-generate-pw"));
		 generate.click();
		 driver.findElement(By.id("createusersub")).click();
		 WebElement messageText =  driver.findElement(By.cssSelector("#message > p:nth-child(1)"));
		 System.out.println("messageText :"+messageText.getText()); 
		  messageText.click();
		   String ActualName = messageText.getText();	
		   String expectedName ="New user created. Edit user";
		   Assert.assertEquals(ActualName, expectedName);
				 
  }
	
  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}