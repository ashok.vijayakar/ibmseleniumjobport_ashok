//**********************************************************************************
 /*TC13:-Posting a job using an external file
Goal: Post a job using details from an external CSV/Excel file
      Open browser with Alchemy Jobs site
      Go to Post a Job page.
      Read job information from an external file and fill the details.
      Click submit.
      Go to Jobs page.
      Confirm job listing is shown on page.*/

//**********************************************************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class projectActivities13 {
	WebDriver driver;
	 WebDriverWait wait;
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	private HSSFCell cell;
	
 @BeforeTest
	  public void beforeTest() {
	    driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs/");
		
	  }
	
@Test
  public void CreateAnewJobListing() throws InterruptedException, IOException {
	WebElement PostAJob = driver.findElement(By.id ("menu-item-26"));
	PostAJob.click();
	driver.findElement(By.cssSelector("a.button")).click();
	driver.findElement(By.id("user_login")).sendKeys("root");
	driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	driver.findElement(By.id("wp-submit")).click();
	
	// Import excel sheet.
    File src=new File("C:\\Users\\AshokVijayakar\\Desktop\\IBM_Data\\PostingAJobUsingAnExternalFile.xls");
     
    // Load the file.
    FileInputStream finput = new FileInputStream(src);
     
    // Load he workbook.
   workbook = new HSSFWorkbook(finput);
     
    // Load the sheet in which data is stored.
    sheet= workbook.getSheetAt(0);
     
    for(int i=1; i<=sheet.getLastRowNum(); i++)
    {
        // Import data for job_title,job_location.
    cell = sheet.getRow(i).getCell(0);
    cell.setCellValue(cell.getStringCellValue()); 
    driver.findElement(By.id("job_title")).sendKeys(cell.getStringCellValue());
    
    cell = sheet.getRow(i).getCell(1);
    cell.setCellValue(cell.getStringCellValue()); 
    driver.findElement(By.id("job_location")).sendKeys(cell.getStringCellValue());
    
    
    cell = sheet.getRow(i).getCell(2);
    cell.setCellValue(cell.getStringCellValue()); 
    driver.findElement(By.id("job_type")).sendKeys(cell.getStringCellValue());
    
   
    cell = sheet.getRow(i).getCell(3);
    cell.setCellValue(cell.getStringCellValue()); 
    driver.findElement(By.id("job_description_ifr")).sendKeys(cell.getStringCellValue());
    
    driver.findElement(By.id("application")).clear();
    cell = sheet.getRow(i).getCell(4);
    cell.setCellValue(cell.getStringCellValue()); 
    driver.findElement(By.id("application")).sendKeys(cell.getStringCellValue());
        
    driver.findElement(By.xpath("//input[contains(@name,'submit_job')]")).click();
    driver.findElement(By.xpath("//*[@id=\"job_preview_submit_button\"]")).click();
    WebElement Jobsubmitted =  driver.findElement(By.cssSelector(".entry-content"));
    Jobsubmitted.getText();
    System.out.println("Job Submitted:"+Jobsubmitted.getText());
    }   
	 }
    @AfterTest
  public void afterTest() {
    	 //driver.close();
  }
    
   }
