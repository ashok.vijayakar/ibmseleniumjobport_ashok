/* Using ReportNG to generate reports
Goal: Rewrite activity 6 to use ReportNG and generate HTML reports
     Generate a report with it ReportNG instead of the usual TestNG reporter for all activities.
     View the result in a browser.*/
//**********************************************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import java.util.List;

public class projectActivities15 {
	 WebDriver driver;
	 WebDriverWait wait;	
	@BeforeTest
	 //Open browser with Alchemy Jobs site 
	  public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs");
		Reporter.log("Test Started");
	  }

	@Test
  public void ApplyToAJob() throws InterruptedException {
		// Navigate to the Jobs page.
		WebElement Jobs = driver.findElement(By.id ("menu-item-24"));
		 Jobs.click();
		 String PageTitle = driver.getTitle();
		 System.out.println("Job Page Title:" +PageTitle );
		 Reporter.log("Title is: " +PageTitle);
		 WebElement KeyWords = driver.findElement(By.id ("search_keywords"));
		 KeyWords.sendKeys("Banking Financial Analyst");
		 //Search for a particular job and wait for listings to show
		  WebElement submit = driver.findElement(By.xpath("//input[contains(@type,'submit')]"));
		   submit.click();
		   Reporter.log("Element Found");
		   driver.manage().window().maximize();
		   //Click and open any one of the jobs listed.
		   List<WebElement> list = driver.findElements(By.tagName("ul"));
		   System.out.println("results:" +list.size()); 
		   
		   for (int i=0;i<list.size();i++)
			 {
				System.out.println(list.get(i).getText());
				
			if(list.get(i).getText().contains("Banking Financial Analyst"))
			{
			list.get(i).click();
			
					break;
				} 
			}
				 	   
		   WebElement submit1 = driver.findElement(By.xpath("//input[contains(@class,'application_button button')]"));
		   submit1.click();
		   WebElement JobAppl = driver.findElement(By.cssSelector(".job_application_email"));
		   System.out.println("job_application_email:" +JobAppl.getText());
		  	} 
      

  @AfterTest
  public void afterTest() {
	  //Close the browser
	  driver.close();
	  Reporter.log("Test Completed");
  }

}
