import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities12 {
	private static final XSSFWorkbook XSSFWorkbook = null;
	WebDriver driver;
	WebDriverWait wait;
	HSSFWorkbook workbook;
    HSSFSheet sheet;
    HSSFCell cell;
	
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	  }
	
	@Test
  public void f() throws IOException, InterruptedException {
		driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click(); 
		// Click on the "Users" menu item
				driver.findElement(By.cssSelector("a.menu-icon-users")).click();
		// Click on the "Add New" button
		driver.findElement(By.cssSelector("#menu-users > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")).click();
		
		// Import excel sheet.
	     File src=new File("C:\\Users\\AshokVijayakar\\Desktop\\IBM_Data\\CreatingAUserUsingAnExternalFile.xls");
	      
	     // Load the file.
	     FileInputStream finput = new FileInputStream(src);
	      
	     // Load he workbook.
	    workbook = new HSSFWorkbook(finput);
	      
	     // Load the sheet in which data is stored.
	     sheet= workbook.getSheetAt(0);
	      
	     for(int i=1; i<=sheet.getLastRowNum(); i++)
	     {
	         // Import data for Username.
	         cell = sheet.getRow(i).getCell(0);
	         cell.setCellValue(cell.getStringCellValue()); 
	         driver.findElement(By.id("user_login")).sendKeys(cell.getStringCellValue());
	        
	         // Import data for Email.
	         cell = sheet.getRow(i).getCell(1);
	         cell.setCellValue(cell.getStringCellValue()); 
	         driver.findElement(By.id("email")).sendKeys(cell.getStringCellValue());
	            
	      // Import data for First Name.
	         cell = sheet.getRow(i).getCell(2);
	         cell.setCellValue(cell.getStringCellValue()); 
	         driver.findElement(By.id("first_name")).sendKeys(cell.getStringCellValue());
	      // Import data for Last Name.
	         cell = sheet.getRow(i).getCell(3);
	         cell.setCellValue(cell.getStringCellValue()); 
	         driver.findElement(By.id("last_name")).sendKeys(cell.getStringCellValue());
	         
	         // Import data for Website.
	         cell = sheet.getRow(i).getCell(4);
	         cell.setCellValue(cell.getStringCellValue()); 
	         driver.findElement(By.id("url")).sendKeys(cell.getStringCellValue());
	         
	      // Import data for Password.
	         cell = sheet.getRow(i).getCell(5);
	         cell.setCellValue(cell.getStringCellValue()); 
	         driver.findElement(By.id("url")).sendKeys(cell.getStringCellValue());
	         
	         Thread.sleep(3000);
			 WebElement generate =  driver.findElement(By.cssSelector(".wp-generate-pw"));
			generate.click();
			 
			 driver.findElement(By.id("createusersub")).click();
			 WebElement messageText =  driver.findElement(By.cssSelector("#message > p:nth-child(1)"));
			 System.out.println("messageText :"+messageText.getText()); 
			  messageText.click();
			   String ActualName = messageText.getText();	
			   String expectedName ="New user created. Edit user";
			   Assert.assertEquals(ActualName, expectedName);
	         
	        }
		
			 	
  }
  
  @AfterTest
  public void afterTest() {
	  //driver.close();
  }

}
