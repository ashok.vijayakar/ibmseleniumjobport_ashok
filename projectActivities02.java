//***************************************
/*TC2. Verify the website heading
Goal: Read the heading of the website and verify the text
a)Open a browser.
b)Navigate to �https://alchemy.hguy.co/jobs�.
c)Get the heading of the webpage.
d)Make sure it matches �Welcome to Alchemy Jobs� exactly.
e)If it matches, close the browser*/
//***************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities02 {
	WebDriver driver;
	WebDriverWait wait ;
  
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		  wait =new WebDriverWait(driver,10);
		//Open a browser
		 driver.get("https://alchemy.hguy.co/jobs");
		  
	  }

	
	@Test
  public void Verifythewebsiteheading() {
		// get the heading of the webpage 
		WebElement headingwebpage  =driver.findElement(By.cssSelector(".entry-title"));
		System.out.println("heading of the webpage:" +headingwebpage.getText());
		String ActualheadingOfThewebpage = headingwebpage.getText();
		//Make sure it matches �Welcome to Alchemy Jobs� exactly.
		String expectedheadingOfThewebpage ="Welcome to Alchemy Jobs";
		Assert.assertEquals(ActualheadingOfThewebpage, expectedheadingOfThewebpage);
  }
	  
  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
