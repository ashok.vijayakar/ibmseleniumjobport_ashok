/*TC1:- Verify the website title
Goal: Read the title of the website and verify the text
a)Open a browser.
b)Navigate to �https://alchemy.hguy.co/jobs�. 
c)Get the title of the website.
d)Make sure it matches �Alchemy Jobs � Job Board Application� exactly.
e)If it matches, close the browser*/
//*****************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities01 {
	WebDriver driver;
	WebDriverWait wait ;
	@BeforeTest
	  public void beforeTest() {
		
	  driver = new FirefoxDriver();
	  wait =new WebDriverWait(driver,10);
	//Open a browser
	  driver.get("https://alchemy.hguy.co/jobs");
	  
	  }
	
	@Test
  public void verifyHomepageTitle() {
		//Make sure it matches �Alchemy Jobs � Job Board Application� exactly.
		String expectedTitle = "Alchemy Jobs � Job Board Application";
		//Get the title of the website
		String actualTitle = driver.getTitle();
		System.out.println("ActualTitle Name:" +actualTitle);
		//matching Actual and expected title
		Assert.assertEquals(actualTitle, expectedTitle);
		  }
  
  @AfterTest
  public void afterTest() {
   //close the browser
	  driver.close();
  }

}
