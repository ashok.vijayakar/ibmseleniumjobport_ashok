//**********************************************************
 /* TC4:- Verify the website�s second heading
Goal: Read the second heading of the website and verify the text
      a)Open a browser.
      b)Navigate to �https://alchemy.hguy.co/jobs�. 
      c)Get the second heading on the page.
      d)Make sure it matches �Quia quis non� exactly.
      e)If it matches, close the browser */

//**********************************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities04 {
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		  wait =new WebDriverWait(driver,10);
		//Open a browser
		 driver.get("https://alchemy.hguy.co/jobs");
	  }
	
	@Test
  public void VerifyTheWebsiteSecondHeading() {
		WebElement Seconheadingwebpage  =driver.findElement(By.cssSelector(".entry-content > h2:nth-child(6)"));
		System.out.println("heading of the webpage:" +Seconheadingwebpage.getText());
		String ActualSecoHeadingOfThewebpage = Seconheadingwebpage.getText();
		//Make sure it matches �Quia quis non� exactly.
		String expectedheadingOfThewebpage ="Quia quis non";
		//matching 
		Assert.assertEquals(ActualSecoHeadingOfThewebpage, expectedheadingOfThewebpage);
  }
  

  @AfterTest
  public void afterTest() {
	  //close the browser
	  driver.close();
  }

}
