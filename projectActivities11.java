//**************************************************************************
/*TC11:-Searching for jobs using XPath
   Goal: Searching for jobs and applying to them using XPath
     Open browser with Alchemy Jobs site and navigate to Jobs page
     Find the Keywords search input field.
     Type in keywords to search for jobs and change the Job type
     Find the filter using XPath and filter job type to show only �Full Time� jobs.
     Find a job listing using XPath and it to see job details.
     Find the title of the job listing using XPath and print it to the console.
     Find and Click on �Apply for job� button.
     Close the browser. */
//****************************************************************************

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;


public class projectActivities11 {
	WebDriver driver;
	WebDriverWait wait;
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs/");
	  }
	
	
	
	@Test
  public void SearchingForJobsAndApplying() {
		WebElement Jobs = driver.findElement(By.id ("menu-item-24"));
		 Jobs.click();
		 String PageTitle = driver.getTitle();
		 System.out.println("Job Page Title:" +PageTitle );
		 WebElement KeyWords = driver.findElement(By.id ("search_keywords"));
		 KeyWords.sendKeys("Data Scientist");
		 
		 WebElement checkboxfreelance = driver.findElement(By.xpath("//*[@id=\"job_type_freelance\"]"));
		 checkboxfreelance.click();
		 System.out.println("The checkbox freelance is displayed: " + checkboxfreelance.isSelected());
		 
		// System.out.println("The checkbox Input is displayed: " + checkboxInput.isSelected());
		 
		 WebElement fulltimecheckbox = driver.findElement(By.xpath("//*[@id=\"job_type_full-time\"]"));
		 System.out.println("The checkbox full-time is displayed: " + fulltimecheckbox.isSelected());

		 WebElement checkboxInternship = driver.findElement(By.xpath("//*[@id=\"job_type_internship\"]"));
		 checkboxInternship.click();
		 System.out.println("The checkbox internship is displayed: " + checkboxInternship.isSelected());
				 
		 WebElement checkboxpartTime = driver.findElement(By.xpath("//*[@id=\"job_type_part-time\"]"));
		 checkboxpartTime.click();
		 System.out.println("The checkbox part-time is displayed: " + checkboxpartTime.isSelected());
		 
		 WebElement checkboxtemporary = driver.findElement(By.xpath("//*[@id=\"job_type_temporary\"]"));
		 checkboxtemporary.click();
		 System.out.println("The checkbox temporary is displayed: " + checkboxtemporary.isSelected());
		 		 
		 //Search for a particular job and wait for listings to show
		 
		  WebElement submit = driver.findElement(By.xpath("//input[contains(@type,'submit')]"));
		   submit.click();
		   driver.manage().window().maximize();
		// Click and open any one of the jobs listed.
		  List<WebElement> list = driver.findElements(By.tagName("ul"));
		 System.out.println("results:" +list.size()); 
		   
		   for (int i=0;i<list.size();i++)
			 {
			System.out.println(list.get(i).getText());
				
		 if(list.get(i).getText().contains("Data Scientist"))
			{
			list.get(i).click();
			
					break;
				} 
			}
				 	   
		      
		   driver.findElement(By.xpath("//input[contains(@class,'application_button button')]")).click();
  }
  

  @AfterTest
  public void afterTest() {
	 driver.close(); 
	  
  }

}
