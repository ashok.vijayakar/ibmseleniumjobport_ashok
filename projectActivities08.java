//**********************************************************************************
/*TC8:-Login into the website�s backend
Goal: Visit the site�s backend and login
   1)Open a browser.
   2)Navigate to �https://alchemy.hguy.co/jobs/wp-admin�. 
   3)Find the username field of the login form and enter the username into that field.
   4)Find the password field of the login form and enter the password into that field.
   5)Find the login button and click it.
   6)Verify that you have logged in.
   7)Close the browser.*/
//***********************************************************************************


import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities08 {
	WebDriver driver;
	WebDriverWait wait;
	
	 @BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
	  }

	 @Test
  public void f() {
		driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click(); 
		String actualTitle = driver.getTitle();
		System.out.println("ActualTitle Name:" +actualTitle);
		String expectedTitle = "Dashboard � Alchemy Jobs � WordPress";
		//matching Actual and expected title
		Assert.assertEquals(actualTitle, expectedTitle);
		
  }
	 
  
  @AfterTest
  public void afterTest() {
	  //driver.close();
  }

}
