//****************************************************************************************
/* TC09:- Create a job listing using the backend
   Goal: Visit the site�s backend and create a job listing
         Open a browser.
         Navigate to �https://alchemy.hguy.co/jobs/wp-admin� and log in.
         Locate the left hand menu and click the menu item that says �Job Listings�.
         Locate the �Add New� button and click it.
         Fill in the necessary details.
         Click the �Publish� button.
         Verify that the job listing was created.
         Close the browser.*/
//****************************************************************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities09_A1 {
	WebDriver driver;
	WebDriverWait wait;
	private int i;
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
		
	  }
	
	@Test
  public void f() {
		driver.findElement(By.id("user_login")).sendKeys("root");
		driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("wp-submit")).click(); 
	 WebElement jobList = driver.findElement(By.xpath("//*[@id=\"menu-posts-job_listing\"]"));
		 jobList.click();
		 
		 
		 List<WebElement> list = driver.findElements(By.tagName("li"));
		   System.out.println("results:" +list.size()); 
		   for (int i=0;i<list.size();i++)
			 {
				System.out.println(list.get(i).getText());
				
			if(list.get(i).getText().contains("Add New"))
			{
			list.get(i).click();
			
					break;
				} 
			}
		   driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		   WebElement dataBlock = driver.findElement(By.id("post-title-0"));
		   dataBlock.click();
		   dataBlock.sendKeys("Java");
		   WebElement application = driver.findElement(By.id("_application"));
		   application.clear();
		   application.sendKeys("ashok.vijayakar@gmail.com");
		   WebElement location = driver.findElement(By.id("_job_location"));
		   location.clear();
		   location.sendKeys("India"); 
		   WebElement companyName = driver.findElement(By.id("_company_name"));
		   companyName.clear();
		   companyName.sendKeys("IBM");
			
		   WebElement publish = driver.findElement(By.cssSelector(".editor-post-publish-panel__toggle"));
		   publish.click();	
		   WebElement publish1 = driver.findElement(By.cssSelector(".editor-post-publish-button"));
		   publish1.click();
		   String Actualbpage = publish1.getText();
		   
			//Make sure it matches �Welcome to Alchemy Jobs� exactly.
			String expectedpage ="Publishing�";
		   		 
			   Assert.assertEquals(Actualbpage, expectedpage);
			    
			   
  }
	
	
  

  @AfterTest
  public void afterTest() {
  }

}
