package suiteExample;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class projectactivitys14_TC {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);
		driver.get("https://alchemy.hguy.co/jobs/wp-admin");
		Reporter.log("Test Started");

	}

	@DataProvider(name = "Authentication")
	public static Object[][] credentials() {
		return new Object[][] { { "root", "pa$$w0rd" } };
	}

	@Test(dataProvider = "Authentication")
	public void loginTestCase(String username, String password) throws InterruptedException {
		WebElement usernameJob = driver.findElement(By.id("user_login"));
		WebElement passwordJob = driver.findElement(By.id("user_pass"));
		usernameJob.sendKeys(username);
		passwordJob.sendKeys(password);
		driver.findElement(By.id("wp-submit")).click();
		Reporter.log("User able to login jobPort");

		// Click on the "Users" menu item
		driver.findElement(By.cssSelector("a.menu-icon-users")).click();
		// Click on the "Add New" button
		driver.findElement(By.cssSelector("#menu-users > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
		
		@DataProvider(name = "AddNewUserFormDetails")
		public static Object[][] AddNewUserForm() {
			return new Object[][] { { "Ashok12345", "ashok1.vijayakar1@gmail.com","Ashok2","vijayakar2","www.ibm.com" } };
		}
		@Test(dataProvider = "AddNewUserFormDetails")
		public void userFormDetailsTestCase(String userlogin, String mail,String firstname,String lastname,String url) throws InterruptedException {
			
		WebElement userName = driver.findElement(By.id("user_login"));
		userName.sendKeys(userlogin);
		//userName.sendKeys("Ashok12345");
		WebElement email = driver.findElement(By.id("email"));
		
		email.sendKeys(mail);
		//email.sendKeys("ashok1.vijayakar1@gmail.com");
		
		WebElement firstName = driver.findElement(By.id("first_name"));
		firstName.sendKeys(firstname);
		//firstName.sendKeys("Ashok2");
		WebElement lastName = driver.findElement(By.id("last_name"));
		lastName.sendKeys(lastname);
		//lastName.sendKeys("vijayakar2");
		WebElement companyurl = driver.findElement(By.id("url"));
		companyurl.sendKeys(url);
		
		//companyurl.sendKeys("www.ibm.com");
		Thread.sleep(3000);
		WebElement generate = driver.findElement(By.cssSelector(".wp-generate-pw"));
		generate.click();
		driver.findElement(By.id("createusersub")).click();
		Reporter.log("Submit Add New User Form");
		WebElement messageText = driver.findElement(By.cssSelector("#message > p:nth-child(1)"));
		System.out.println("messageText :" + messageText.getText());
		messageText.click();
		String ActualName = messageText.getText();
		String expectedName = "New user created. Edit user";
		Assert.assertEquals(ActualName, expectedName);
		Reporter.log("ActualName and expectedName are equals");
	}

	@AfterClass
	public void afterClass() {
		// Close browser
		driver.close();
		Reporter.log("Test Completed");
	}

}