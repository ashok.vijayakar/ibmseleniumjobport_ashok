//**********************************************************************************
 /*TC07:-Create a new job listing
Goal: Create a new job listing
      a)Open a browser.
      b)Navigate to �https://alchemy.hguy.co/jobs�
      c)Locate the navigation menu and click the menu item that says �Post a Job�
      d)Fill in the necessary details and click the button that says �Preview�.
      e)Click on the button that says �Submit Listing�.
      f)Verify that the job listing was posted by visiting the jobs page.
      g)Close the browser.*/

//**********************************************************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class projectActivities07 {
	WebDriver driver;
	 WebDriverWait wait;
	
 @BeforeTest
	  public void beforeTest() {
	    driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs/");
		
	  }
	
@Test
  public void CreateAnewJobListing() throws InterruptedException {
	WebElement PostAJob = driver.findElement(By.id ("menu-item-26"));
	PostAJob.click();
	driver.findElement(By.cssSelector("a.button")).click();
	driver.findElement(By.id("user_login")).sendKeys("root");
	driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
	driver.findElement(By.id("wp-submit")).click();
	driver.findElement(By.id("job_title")).sendKeys("Testing");
    driver.findElement(By.id("job_location")).sendKeys("India");
    driver.findElement(By.id("job_type")).sendKeys("Full Time");
    driver.findElement(By.id("job_description_ifr")).sendKeys("Description");
    driver.findElement(By.id("application")).clear();
    driver.findElement(By.id("application")).sendKeys("ashok.vijayakar@gmail.com");
    driver.findElement(By.xpath("//input[contains(@name,'submit_job')]")).click();
    driver.findElement(By.xpath("//*[@id=\"job_preview_submit_button\"]")).click();
    WebElement Jobsubmitted =  driver.findElement(By.cssSelector(".entry-content"));
    Jobsubmitted.getText();
    System.out.println("Job Submitted:"+Jobsubmitted.getText());
         
	 }
    @AfterTest
  public void afterTest() {
    	 driver.close();
  }
    
   }
