//**********************************
/*Get the url of the header image
Goal: Print the url of the header image to the console
      a) Open a browser.
      b) Navigate to �https://alchemy.hguy.co/jobs�. 
      c) Get the url of the header image.
      d) Print the url to the console.
      e) Close the browser.*/

//*****************************

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities03 {
	
	WebDriver driver;
	WebDriverWait wait ;

	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		  wait =new WebDriverWait(driver,10);
		//Open a browser
		 driver.get("https://alchemy.hguy.co/jobs");
		  
	  }
	
	@Test
  public void urlOfTheHeaderImage() {
		// get the heading of the webpage 
				WebElement imag =driver.findElement(By.cssSelector(".attachment-large"));
				String src = imag.getAttribute("src");
	    		System.out.println("Print the url of the header image to the console:" +src);
	
  }
  
  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
