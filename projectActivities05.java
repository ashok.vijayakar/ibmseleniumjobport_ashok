//***************************************************************************
/*TC 5:-Navigate to another page
Goal: Navigate to the �Jobs� page on the site.
      a)Open a browser.
      b)Navigate to �https://alchemy.hguy.co/jobs�. 
      c)Find the navigation bar.
      d)Select the menu item that says �Jobs� and click it.
      e)Read the page title and verify that you are on the correct page.
      f)Close the browser. */

//***************************************************************************
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class projectActivities05 {
   WebDriver driver;
   WebDriverWait wait;	
	
	@BeforeTest
	  public void beforeTest() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,10);
		driver.get("https://alchemy.hguy.co/jobs");
		
		 }
	
	@Test
  public void verifyThatYouAreOnTheCorrectPage() {
         WebElement Jobs = driver.findElement(By.id ("menu-item-24"));
		 Jobs.click();
		 String ActualPageTitle = driver.getTitle();
		 System.out.println("Read the page title:" +ActualPageTitle);
		 String ExpectedPageTitle= "Jobs � Alchemy Jobs" ;
		 Assert.assertEquals(ActualPageTitle,ExpectedPageTitle);
		 //verify the correct page using assert class :-  is it correct ? 
		 
		
	  }
  

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
